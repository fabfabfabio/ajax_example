package com.db.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Test extends HttpServlet {
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		
		String paramExo = request.getParameter( "exo" );
		//System.out.println(paramExo);
		if(paramExo!=null) {
			choixExo(Integer.valueOf(paramExo));
		}
		String message = "Transmission de variables : OK ! " + paramExo;
		request.setAttribute( "test", message );
		this.getServletContext().getRequestDispatcher( "/WEB-INF/test.jsp" ).forward( request, response );
		
	}
	
	
	public static void exo1() {
		int a,b;
		a=1;
		b=a+3;
		a=3;
		System.out.println("A=" + a + " B=" + b);
	}
	
	public static void exo2() {
		int a,b,c;
		a=5;
		b=3;
		c=a+b;
		a=2;
		c=b-a;
		System.out.println("A=" + a + " B=" + b + " C=" + c );
	}
	
	public static void exo3() {
		int a,b;
		a=5;
		b=a+4;
		a=a+1;
		b=a-4;
		System.out.println("A=" + a + " B=" + b);
	}
	
	public static void exo4() {
		int a,b,c;
		a=3;
		b=10;
		c=a+b;
		b=a+b;
		a=c;
		System.out.println("A=" + a + " B=" + b + " C=" + c );
	}
	
	public static void exo5() {
		int a,b;
		a=5;
		b=2;
		a=b;
		b=a;
		System.out.println("A=" + a + " B=" + b);
	}
	
	public static void exo6() {
		int a=5;
		int b=7;
		int c;
		c=a;
		a=b;
		b=c;
		System.out.println("A=" + a + " B=" + b);
	}
	
	public static void exo7() {
		int a=5;
		int b=7;
		int c=9;
		int d;
		d=b;
		b=a;
		a=c;
		c=d;
		System.out.println("A=" + a + " B=" + b + " C=" + c);
	}
	
	public static void exo9() {
		String a,b,c;
		a="423";
		b="12";
		c=a+b;
		System.out.println("A=" + a + " B=" + b + " C=" + c);
	}
	
	public static void exo10() {
		int val, doub;
		val=231;
		doub=val*2;
		System.out.println("val=" + val + " doub=" + doub );
	}
	
	public static void exo11() {
		int val;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez val");
		val=sc.nextInt();
		System.out.println("val�=" + val*val);
	}
	
	public static void exo12() {
		double ht, txTva, na, ttc;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez le prix hors taxe");
		ht=sc.nextDouble();
		System.out.println("Saissez le nb d'articles");
		na=sc.nextDouble();
		System.out.println("Saissez le taux tva");
		txTva=sc.nextDouble();
		ttc=na*ht*(1+txTva);
		System.out.println("Prix TTC=" + ttc);
	}
	
	public static void exo1316() {
		int val;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez val");
		val=sc.nextInt();
		System.out.print("val est");
		if(val>0) {
			System.out.print(" positif");
		}else if(val<0) {
			System.out.print(" negatif");
		}else {
			System.out.print(" nul");
		}
	}
	
	public static void exo1417() {
		int val, val2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez val");
		val=sc.nextInt();
		System.out.println("Saissez val2");
		val2=sc.nextInt();
		System.out.print("Le produit est");
		if((val>0 && val2>0) || (val<0 && val2<0)) {
			System.out.print(" positif");
		}else if(val!=0 && val2!=0) {
			System.out.print(" negatif");
		}else {
			System.out.print(" nul");
		}
	}
	
	public static void exo18() {
		int age;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez l'age de l'enfant");
		age=sc.nextInt();
		System.out.print("L'enfant est ");
		if(age>=12) {
			System.out.print(" poussin");
		}else if(age>=10) {
			System.out.print(" minime");
		}else if(age>=8){
			System.out.print(" pupille");
		}else if(age>=6){
			System.out.print(" poussin");
		}else {
			System.out.print(" dans aucune de ces cat�gories");
		}
	}
	
	public static void exo19() {
		int val;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez une valeur");
		val=sc.nextInt();
		for(int i=val+1;i<val+10;i++) {
			System.out.println(i);
		}
	}
	
	public static void exo20() {
		int val,sum;
		sum = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez une valeur");
		val=sc.nextInt();
		for(int i=1;i<=val;i++) {
			sum+=i;
		}
		System.out.println("La somme est de " + sum);
	}
	
	public static void exo21() {
		int val,fact;
		fact = 1;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saissez une valeur");
		val=sc.nextInt();
		for(int i=1;i<=val;i++) {
			fact*=i;
		}
		System.out.println("La factorielle est de " + fact);
	}
	
	public static int max(int[] tab) {
		int max = tab[0];
		for(int i=1;i<(tab.length);i++) {
			if(tab[i]>max) {
				max = tab[i];
			}
		}
		return max;
	}
	
	public static void exo22() {
		int[] tab = new int[20];
		Scanner sc = new Scanner(System.in);
		
		
		for(int i=0;i<=19;i++) {
			System.out.println("Saissez une valeur");
			tab[i]=sc.nextInt();
		}
		System.out.println("Le max est: " + max(tab));
	}
	
	public static void exo23() {
		int cmpt = 0;
		int val,max;
		max=0;
		Scanner sc = new Scanner(System.in);
		val = sc.nextInt();
		while(val!=0) {
			val = sc.nextInt();
			if(val>max) {
				max=val;
			}
			cmpt++;
		}
		System.out.println("Le max est: " + max);
		
	}
	
	public static void exo24() {
		String sexe;
		int age;
		boolean imposable;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entrez votre age");
		age = sc.nextInt();
		System.out.println("Entrez votre sexe (homme ou femme");
		sexe = sc.next();
		
		if(sexe=="femme" && age>=18 && age<35) {
			imposable=true;
		}else if(age>20) {
			imposable=true;
		}else {
			imposable=false;
		}
		
		if(imposable) {
			System.out.println("Vous etes imposable");
		}else {
			System.out.println("Vous n'etes pas imposable");
		}
	}
	
	public static void exo27() {
	int ecrit, oral;
		
		System.out.println("Saissez la note de l'�crit");
		Scanner sc = new Scanner(System.in);
		ecrit = sc.nextInt();
		System.out.println("Saissez la note de l'oral");
		oral = sc.nextInt();
		
		if((ecrit+oral*2)/3 >=10) {
			System.out.println("Vous validez le module");
		}else {
			System.out.println("Vous ne validez pas le module");
		}
	}
	
	public static void exo28() {
int nbLignes;
		
		System.out.println("Saissez le nombre de lignes voulus");
		Scanner sc = new Scanner(System.in);
		nbLignes = sc.nextInt();
		for(int i=1;i<=nbLignes;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}
	
	public static void exo31() {
		String[] tab = new String[6];
		tab[0]="a";
		tab[1]="e";
		tab[2]="i";
		tab[3]="o";
		tab[4]="u";
		tab[5]="y";
	}
	
	public static void exo32() {
		int[] tab = new int[9];
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<=8;i++) {
			tab[i]=sc.nextInt();
		}
	}
	
	public static void exo33() {
		int[] tab;
		int val, pos, neg;
		pos=neg=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le nb de valeurs que vous voulez saisir");
		val=sc.nextInt();
		tab= new int[val];
		for(int i=0;i<val;i++) {
			tab[i]=sc.nextInt();
			if(tab[i]>=0) {
				pos+=1;
			}else {
				neg+=1;
			}
		}
		
		System.out.println("Il y a " + pos + " valeurs positives et " + neg + " valeurs negatives");
	}
	
	public static void exo34() {
		int[] tab;
		int val, sum;
		sum=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le nb de valeurs que vous voulez saisir");
		val=sc.nextInt();
		tab= new int[val];
		for(int i=0;i<val;i++) {
			tab[i]=sc.nextInt();
			sum+=tab[i];
		}
		
		System.out.println("La somme est de " + sum);
	}
	
	public static void exo35() {
		int[] tab;
		int[] tab2;
		int[] tab3;
		int val;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entrez le nb de valeurs que vous voulez saisir dans le premier tableau");
		val=sc.nextInt();
		tab= new int[val];
		for(int i=0;i<val;i++) {
			tab[i]=sc.nextInt();
		}
		
		System.out.println("Entrez le nb de valeurs que vous voulez saisir dans le deuxieme tableau");
		val=sc.nextInt();
		tab2= new int[val];
		for(int i=0;i<val;i++) {
			tab2[i]=sc.nextInt();
		}
		
		tab3= new int[val];
		for(int i=0;i<val;i++) {
			tab3[i]=tab[i]+tab2[i];
			System.out.println(tab3[i]);
		}
		
		
	}
	
	public static void exo36() {
		int[] tab;
		int[] tab2;
		int smurf;
		int val;
		smurf=0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entrez le nb de valeurs que vous voulez saisir dans le premier tableau");
		val=sc.nextInt();
		tab= new int[val];
		for(int i=0;i<val;i++) {
			tab[i]=sc.nextInt();
		}
		
		System.out.println("Entrez le nb de valeurs que vous voulez saisir dans le deuxieme tableau");
		val=sc.nextInt();
		tab2= new int[val];
		for(int i=0;i<val;i++) {
			tab2[i]=sc.nextInt();
		}
		
		for(int i=0;i<tab.length;i++) {
			for(int j=0;j<tab2.length;j++) {
				smurf+=tab[i]*tab2[j];
			}
		}
		
		System.out.println("Le smurf est de " + smurf);
	}
	
	public static void exo40() {
		int tab[][];
		int max, dim1, dim2;
		max=0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entrez la premiere dim du tableau");
		dim1=sc.nextInt();
		System.out.println("Entrez la deuxieme dim du tableau");
		dim2=sc.nextInt();
		tab = new int[dim1][dim2];
		
		for(int i=0;i<tab.length;i++) {
			for(int j=0;j<tab[0].length;j++) {
				System.out.println("Entrez tab[" + i + "][" + j + "]");
				tab[i][j]=sc.nextInt();
				if(i==0 && j==0) {
					max=tab[i][j];
				}
				if(tab[i][j]>max) {
					max=tab[i][j];
				}
			}
		}
		System.out.println("Le maximum est: " + max);
		
	}
	
	public static void choixExo(int exo) {
		switch(exo) {
		  case 1:
		    exo1();
		    break;
		  case 2:
			    exo2();
			    break;
		  case 3:
			    exo3();
			    break;
		  case 4:
			    exo4();
			    break;
		  case 5:
			    exo5();
			    break;
		  case 6:
			    exo6();
			    break;
		  case 7:
			    exo7();
			    break;
		  case 9:
			    exo9();
			    break;
		  case 10:
			    exo10();
			    break;
		  case 11:
			    exo11();
			    break;
		  case 12:
			    exo12();
			    break;
		  case 13:
			    exo1316();
			    break;
		  case 14:
			    exo1417();
			    break;
		  case 16:
			    exo1316();
			    break;
		  case 17:
			    exo1417();
			    break;
		  case 18:
			    exo18();
			    break;
		  case 19:
			    exo19();
			    break;
		  case 20:
			    exo20();
			    break;
		  case 21:
			    exo21();
			    break;
		  case 22:
			    exo22();
			    break;
		  case 23:
			    exo23();
			    break;
		  case 24:
			    exo24();
			    break;
		  case 27:
			    exo27();
			    break;
		  case 28:
			    exo28();
			    break;
		  case 31:
			    exo31();
			    break;
		  case 32:
			    exo32();
			    break;
		  case 33:
			    exo33();
			    break;
		  case 34:
			    exo34();
			    break;
		  case 35:
			    exo35();
			    break;
		  case 36:
			    exo36();
			    break;
		  case 40:
			    exo40();
			    break;
			    
		}
	}
	
}