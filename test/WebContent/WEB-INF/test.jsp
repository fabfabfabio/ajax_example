<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
	<html>
		<script type="text/javascript">
		//Fonctions AJAX
			function getXMLHttpRequest() {
				var xhr = null;
				if (window.XMLHttpRequest || window.ActiveXObject) {
					if (window.ActiveXObject) {
						try {
							xhr = new ActiveXObject("Msxml2.XMLHTTP");
						} catch(e) {
							xhr = new ActiveXObject("Microsoft.XMLHTTP");
						}
					} else {
						xhr = new XMLHttpRequest(); 
					}
				} else {
					alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
					return null;
				}
				return xhr;
			}


			function choixExo(id) {
				var xhr = getXMLHttpRequest();
				var url = "toto?exo=" + escape(id);
				alert(id);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
						document.getElementById('main_content').innerHTML = xhr.responseText; // Données textuelles récupérées
						//test
					}
				};
				xhr.open("GET", url, true);
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("exo=" + id);
			}
		</script>
		
		<head>
		<meta charset="utf-8">
		<title>Insert title here</title>
		</head>
		<body id="main_content">
			<p>Ceci est une page générée depuis une JSP.
				<% 
					String attribut = (String) request.getAttribute("test");
	            	out.println( attribut ); 
	            %>
			</p>
        <p>
        	<select id="selec" onChange="choixExo(document.getElementById('selec').value);">
            <% 
            for(int i=1;i<=40;i++){
            	if(i!=8 && i!=15 && i!=25 && i!=26 && i!=29 && i!=30 && i!=37 && i!=38 && i!=39)
            	out.println("<option value=\""+i+"\" onClick=\";\"" + "class=\"exercices\" id=\""+i+"\" > Exo"+ i  +"</option>");
            }
            
            
            %>
            </select>
        </p>
        
		</body>
	</html>